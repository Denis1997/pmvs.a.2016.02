package com.example.shilo.pmvsa2016shilo02;

import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

public class Lab2MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab2_main);
        initElements();
    }

    private void initElements()
    {
        editText = (EditText)findViewById(R.id.editTextInputNumberField);
        buttonGuess = (Button)findViewById(R.id.buttonAnswerGuess);
        textViewHint = (TextView)findViewById(R.id.textViewHint);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        myNumber = makeANumber();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                editText.setText(Integer.toString(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private EditText editText;
    private Button buttonGuess;
    private TextView textViewHint;
    private SeekBar seekBar;

    private int myNumber;
    private int leftBoundary = 1;
    private int rightBoundary = 100;
    private Random rand = new Random();
    private boolean isGameFinished = false;

    private int makeANumber()
    {
        return rand.nextInt(rightBoundary - leftBoundary + 1) + leftBoundary;
    }

    public void onButtonGuessClick(View view)
    {
        if (isGameFinished){
            myNumber = makeANumber();
            isGameFinished = false;
            buttonGuess.setText(getString(R.string.input_value));
            editText.setText("");
            return;
        }
        int guess;
        try {
            guess = Integer.parseInt(editText.getText().toString());
        }
        catch (NumberFormatException nfe) {
            textViewHint.setText(getString(R.string.error));
            return;
        }
        if (guess > rightBoundary)
        {
            textViewHint.setText(getString(R.string.aheadRightBoundary));
            return;
        }
        if (guess < leftBoundary)
        {
            textViewHint.setText(getString(R.string.behindRightBoundary));
            return;
        }
        if (guess > myNumber)
            textViewHint.setText(getString(R.string.ahead));
        else if (guess < myNumber)
            textViewHint.setText(getString(R.string.behind));
        else if (guess == myNumber) {
            textViewHint.setText(getString(R.string.hit));
            isGameFinished = true;
            buttonGuess.setText(getString(R.string.play_more));
        }
    }
}
